﻿using Broker.Libaries.Data;
using Broker.Site.Middleware;
using Broker.Site.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Net.Http;

namespace Broker.Site
{
    public class Startup
    {
        private readonly IConfiguration _configuartion;

        public Startup(IConfiguration configuartion)
        {
            _configuartion = configuartion;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<BrokerDbContext>(o => o.UseSqlServer(_configuartion.GetConnectionString("BrokerConnection")));

            services.AddHostedService<MessagingErrorQueueBackgroundService>();
            services.AddHostedService<MessagingQueueBackgroundService>();

            services.AddSingleton<ScopedServiceAccessor>();
            services.AddSingleton<MessagingQueue>();
            services.AddSingleton<MessagingErrorQueue>();

            services.AddSingleton<HttpClient>();
            services.AddScoped<MessagingProcessor>();

            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseMiddleware<ExceptionHandleMiddleware>();

            if (!env.IsDevelopment())
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseMvcWithDefaultRoute();
        }
    }
}
