﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Broker.Site
{
    public static class HttpRequestExtensions
    {
        private const string _jsonContentType = "application/json";

        public static async Task<string> ReadBodyAsync(this HttpRequest request, ILogger logger = null)
        {
            if (_jsonContentType == request.ContentType)
            {
                return await _readJsonBody(request);
            }
            else if(request.HasFormContentType)
            {
                return await _readFormBodyAsJson(request);
            }
            else
            {
                logger?.LogInformation($"Reading body with ContentType: {request.ContentType} as text");
                return await ReadBodyAsString(request);
            }
        }

        public static async Task<string> ReadBodyAsString(this HttpRequest request)
        {
            if (request.Body.CanSeek)
            {
                request.Body.Seek(0, SeekOrigin.Begin);
            }
            
            using (StreamReader reader = new StreamReader(request.Body, Encoding.UTF8))
            {
                return await reader.ReadToEndAsync();
            }
        }

        private static async Task<string> _readJsonBody(HttpRequest request)
        {
            var json = await ReadBodyAsString(request);
            return JObject.Parse(json).ToString(Formatting.None);
        }

        private static Task<string> _readFormBodyAsJson(HttpRequest request)
        {
            JObject obj = new JObject();
            foreach (var item in request.Form)
            {
                if (item.Value.Count == 1)
                {
                    obj.Add(item.Key, (string)item.Value);
                }
                else
                {
                    obj.Add(item.Key, new JArray(item.Value.ToArray()));
                }
            }

            return Task.FromResult(obj.ToString(Formatting.None));
        }
    }
}
