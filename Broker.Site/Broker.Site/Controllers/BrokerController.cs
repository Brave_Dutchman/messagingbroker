﻿using Broker.Libaries.Data;
using Broker.Libaries.Data.Models;
using Broker.Site.Models.Queries;
using Broker.Site.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Broker.Site.Controllers
{

    [ApiController]
    [Route("api/broker/{apiKey}")]
    public class BrokerController : Controller
    {
        private readonly BrokerDbContext _dbContext;
        private readonly ILogger _logger;
        private readonly MessagingQueue _messagingQueue;

        public BrokerController(BrokerDbContext dbContext, ILogger<BrokerController> logger, MessagingQueue messagingQueue)
        {
            _dbContext = dbContext;
            _logger = logger;
            _messagingQueue = messagingQueue;
        }
        
        [HttpPost("message/{key}")]
        public async Task<IActionResult> Message(string apiKey, string key)
        {
            return await _handleInputRequestAsync(apiKey, key);
        }

        private async Task<IActionResult> _handleInputRequestAsync(string apiKey, string key)
        {
            var publisher = await _validateApiKey(apiKey);
            if (publisher == null)
            {
                _logger.LogWarning($"No provider found with apikey '{apiKey}'");
                return Forbid();
            }

            var content = await Request.ReadBodyAsync(_logger);
            var message = new Message
            {
                Key = key,
                CreatedOn = DateTime.Now,
                Content = content,
                ContentType = Request.ContentType,
                PublisherId = publisher.Id
            };

            _dbContext.Add(message);
            await _dbContext.SaveChangesAsync();

            await _messagingQueue.PushMessageAsync(message, _dbContext);

            return Ok();
        }

        private async Task<PublisherQueryModel> _validateApiKey(string apiKey)
        {
            var publisher = await _dbContext.Publishers
                .Where(p => p.ApiKey == apiKey)
                .Select(p => new PublisherQueryModel { Id = p.Id })
                .FirstOrDefaultAsync();

            return publisher;
        }
    }
}
