﻿using Broker.Libaries.Data;
using Broker.Libaries.Data.Models;
using Broker.Site.Models.Requests;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;

namespace Broker.Site.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class PublisherController : Controller
    {
        private readonly BrokerDbContext _dbContext;

        public PublisherController(BrokerDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        [HttpPost("create")]
        public async Task<IActionResult> Create(PublisherCreateRequest input)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var now = DateTime.Now;
            var publisher = new Publisher
            {
                Name = input.Name,
                ApiKey = Guid.NewGuid().ToString("N") + Guid.NewGuid().ToString("N"),
                CreatedOn = now,
                UpdatedOn = now
            };

            _dbContext.Add(publisher);

            await _dbContext.SaveChangesAsync();

            return Json(publisher, new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore});
        }
    }
}
