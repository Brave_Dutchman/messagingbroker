﻿using Broker.Libaries.Data;
using Broker.Site.Apis;
using Broker.Site.Models.Queries;
using Broker.Site.Models.Requests;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System.Linq;
using System.Threading.Tasks;

namespace Broker.Site.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class RequeueController : Controller
    {
        private readonly BrokerDbContext _dbContext;
        private readonly ILogger _logger;
        private readonly RequeueQueueApi _requeueQueueApi;

        public RequeueController(BrokerDbContext dbContext, ILogger<RequeueController> logger, RequeueQueueApi requeueQueueApi)
        {
            _dbContext = dbContext;
            _logger = logger;
            _requeueQueueApi = requeueQueueApi;
        }

        [HttpPost("error")]
        public async Task<IActionResult> Error(RequeueErrorRequest model)
        {
            var publisher = await _validateApiKey(model.ApiKey);
            if (publisher == null)
            {
                _logger.LogWarning($"No provider found with apikey '{model.ApiKey}'");
                return Forbid();
            }

            await _requeueQueueApi.RequeueError(publisher, model);

            return Ok();
        }

        [HttpPost("unsend")]
        public async Task<IActionResult> Unsend(RequeueUnsendRequest model)
        {
            var subscription = await _dbContext.Subscriptions
                .Where(s => s.Name == model.SubscriptionName)
                .Select(s => new SubscriberQueryModel { Id = s.Id })
                .SingleOrDefaultAsync();

            if (subscription == null)
            {
                return NotFound();
            }

            await _requeueQueueApi.RequeueUnsend(subscription);

            return Ok();
        }

        private async Task<PublisherQueryModel> _validateApiKey(string apiKey)
        {
            var publisher = await _dbContext.Publishers
                .Where(p => p.ApiKey == apiKey)
                .Select(p => new PublisherQueryModel { Id = p.Id })
                .FirstOrDefaultAsync();

            return publisher;
        }
    }
}
