﻿using Broker.Libaries.Data;
using Broker.Libaries.Data.Models;
using Broker.Site.Apis;
using Broker.Site.Models.Queries;
using Broker.Site.Models.Requests;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Linq;
using System.Threading.Tasks;

namespace Broker.Site.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class SubscriptionController : Controller
    {
        private readonly BrokerDbContext _dbContext;
        private readonly RequeueQueueApi _requeueQueueApi;

        public SubscriptionController(BrokerDbContext dbContext, RequeueQueueApi requeueQueueApi)
        {
            _dbContext = dbContext;
            _requeueQueueApi = requeueQueueApi;
        }

        [HttpPost("create")]
        public async Task<IActionResult> Create(SubscriptionCreateRequest input)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (string.IsNullOrWhiteSpace(input.ApiKey))
            {
                input.ApiKey = null;
            }

            if (input.ApiKey == null && input.Method == ApiKeyMethod.None)
            {
                ModelState.AddModelError(nameof(input.Method), "ApiKey set but no method specified");
                return BadRequest(ModelState);
            }

            var subscription = (new Subscription
            {
                Name = input.Name,
                Key = input.Key,
                Url = input.Url,
                ApiKey = input.ApiKey,
                IsActive = input.IsActive,
                ApiKeyMethod = input.ApiKey == null ? ApiKeyMethod.None : input.Method
            });

            _dbContext.Add(subscription);

            await _dbContext.SaveChangesAsync();

            return Json(subscription, new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore });
        }

        [HttpPost("activate")]
        public async Task<IActionResult> Activate(SubScriptionActivateRequest model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var subscription = _dbContext.Subscriptions.SingleOrDefault(s => s.Name == model.Name);
            subscription.IsActive = true;

            await _dbContext.SaveChangesAsync();

            if (model.QueueUnsend)
            {
                await _requeueQueueApi.RequeueUnsend(new SubscriberQueryModel { Id = subscription.Id });
            }

            return Ok();
        }

        [HttpPost("deactivate")]
        public async Task<IActionResult> Deactivate(SubScriptionDeactivateRequest model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var subscription = _dbContext.Subscriptions.SingleOrDefault(s => s.Name == model.Name);
            subscription.IsActive = false;

            await _dbContext.SaveChangesAsync();

            return Ok();
        }
    }
}
