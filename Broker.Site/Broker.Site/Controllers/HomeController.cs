﻿using Microsoft.AspNetCore.Mvc;

namespace Broker.Site.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return Ok("Running");
        }
    }
}
