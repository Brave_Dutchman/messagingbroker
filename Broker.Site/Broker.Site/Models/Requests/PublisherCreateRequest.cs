﻿using System.ComponentModel.DataAnnotations;

namespace Broker.Site.Models.Requests
{
    public class PublisherCreateRequest
    {
        [Required]
        public string Name { get; set; }
    }
}
