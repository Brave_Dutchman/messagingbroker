﻿namespace Broker.Site.Models.Requests
{
    public class RequeueUnsendRequest
    {
        public string SubscriptionName { get; set; }
    }
}
