﻿using System.ComponentModel.DataAnnotations;

namespace Broker.Site.Models.Requests
{
    public class SubScriptionDeactivateRequest
    {
        [Required]
        public string Name { get; set; }
    }
}
