﻿using System;

namespace Broker.Site.Models.Requests
{
    public class RequeueErrorRequest
    {
        public string ApiKey { get; set; }

        public string Key { get; set; }

        public DateTime? From { get; set; }
    }
}
