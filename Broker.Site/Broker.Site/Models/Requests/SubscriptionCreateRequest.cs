﻿using Broker.Libaries.Data.Models;
using System.ComponentModel.DataAnnotations;

namespace Broker.Site.Models.Requests
{
    public class SubscriptionCreateRequest
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public string Key { get; set; }

        [Required]
        public string Url { get; set; }

        public string ApiKey { get; set; }

        public ApiKeyMethod Method { get; set; }

        public bool IsActive { get; set; } = true;
    }
}
