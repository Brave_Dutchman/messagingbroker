﻿using System.ComponentModel.DataAnnotations;

namespace Broker.Site.Models.Requests
{
    public class SubScriptionActivateRequest
    {
        [Required]
        public string Name { get; set; }

        public bool QueueUnsend { get; set; }
    }
}
