﻿using Broker.Libaries.Data;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Threading.Tasks;

namespace Broker.Site.Services
{
    public class ScopedServiceAccessor
    {
        private readonly IServiceProvider _serviceProvider;

        public ScopedServiceAccessor(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public async Task DbContextAsync(Func<BrokerDbContext, Task> databaseFunc)
        {
            using (var scope = _serviceProvider.CreateScope())
            {
                var dbContext = scope.ServiceProvider.GetRequiredService<BrokerDbContext>();
                await databaseFunc(dbContext);
            }
        }
    }
}
