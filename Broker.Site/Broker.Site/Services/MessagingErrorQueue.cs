﻿using Broker.Libaries.Data.Models;
using Broker.Libaries.Data.Models.Enums;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Broker.Site.Services
{
    public class MessagingErrorQueue
    {
        private readonly ConcurrentQueue<QueueItem> _messages = new ConcurrentQueue<QueueItem>();
        private readonly SemaphoreSlim _signal = new SemaphoreSlim(0);
        private readonly ScopedServiceAccessor _scopedServiceAccessor;

        public MessagingErrorQueue(ScopedServiceAccessor scopedServiceAccessor)
        {
            _scopedServiceAccessor = scopedServiceAccessor;
        }

        public async Task PushAsync(QueueItem item)
        {
            if (item == null)
            {
                throw new ArgumentNullException(nameof(item));
            }

            item.ErrorCount++;
            item.LastErrorAt = DateTime.Now;

            await _scopedServiceAccessor.DbContextAsync(async (dbContext) =>
            {
                var queuedMessage = new QueuedMessage
                {
                    Id = item.Id,
                };

                dbContext.Attach(queuedMessage);
                queuedMessage.ErrorCount = item.ErrorCount;
                queuedMessage.LastErrorOn = item.LastErrorAt;

                await dbContext.SaveChangesAsync();
            });

            _messages.Enqueue(item);
            _signal.Release();
        }

        public async Task PushErrorAsync(QueueItem item)
        {
            await _scopedServiceAccessor.DbContextAsync(async (dbContext) =>
            {
                var queuedMessage = new QueuedMessage
                {
                    Id = item.Id,
                };

                dbContext.Attach(queuedMessage);
                queuedMessage.Status = QueuedMessageStatus.Error;

                var uncompletedSubscriptions = await dbContext.QueuedMessageSubscriptions
                    .Where(qms => qms.QueuedMessageId == item.Id)
                    .ToArrayAsync();

                foreach (var subscription in uncompletedSubscriptions)
                {
                    switch (subscription.Status)
                    {
                        case MessageSubscriptionStatus.Unsend:
                        case MessageSubscriptionStatus.Completed:
                            // No operation
                            break;
                        default:
                            subscription.Status = MessageSubscriptionStatus.Error;
                            break;
                    }
                }

                await dbContext.SaveChangesAsync();
            });
        }

        public async Task<QueueItem> DequeueAsync(CancellationToken cancellationToken)
        {
            await _signal.WaitAsync(cancellationToken);
            _messages.TryDequeue(out var message);

            return message;
        }
    }
}
