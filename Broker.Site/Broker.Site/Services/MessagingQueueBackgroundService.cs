﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Broker.Site.Services
{
    public class MessagingQueueBackgroundService : BackgroundService
    {
        private readonly ILogger _logger;
        private readonly IServiceProvider _serviceProvider;
        private readonly MessagingQueue _messagingQueue;
        private readonly MessagingErrorQueue _messagingErrorQueue;

        public MessagingQueueBackgroundService(MessagingQueue messagingQueue, MessagingErrorQueue messagingErrorQueue,
            ILogger<MessagingQueueBackgroundService> logger, IServiceProvider serviceProvider)
        {
            _messagingQueue = messagingQueue;
            _messagingErrorQueue = messagingErrorQueue;
            _logger = logger;
            _serviceProvider = serviceProvider;
        }

        protected async override Task ExecuteAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("MessagingProcessor is starting.");


            while (!cancellationToken.IsCancellationRequested)
            {
                try
                {
                    var queueItem = await _messagingQueue.DequeueAsync(cancellationToken);
                    if (queueItem == null)
                    {
                        continue;
                    }

                    try
                    {
                        using (var scope = _serviceProvider.CreateScope())
                        {
                            var processor = scope.ServiceProvider.GetRequiredService<MessagingProcessor>();
                            await processor.ProcessAsync(queueItem, cancellationToken);
                        }
                    }
                    catch (Exception ex) when (!(ex is TaskCanceledException))
                    {
                        _logger.LogError(ex, $"Error while sending message: '{queueItem.Message.Id}' and queue item: '{queueItem.Id}'");
                        await _messagingErrorQueue.PushAsync(queueItem);
                    }
                }
                catch (TaskCanceledException ex)
                {
                    _logger.LogInformation(ex, "MessagingProcessor Task Canceled");
                    break;
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"Error occurred executing.");
                }
            }
        }
    }
}
