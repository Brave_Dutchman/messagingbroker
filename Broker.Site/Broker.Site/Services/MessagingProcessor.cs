﻿using Broker.Libaries.Data;
using Broker.Libaries.Data.Models;
using Broker.Libaries.Data.Models.Enums;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Broker.Site.Services
{
    public class MessagingProcessor
    {
        private readonly BrokerDbContext _dbContext;
        private readonly HttpClient _httpClient;

        public MessagingProcessor(BrokerDbContext dbContext, HttpClient httpClient)
        {
            _dbContext = dbContext;
            _httpClient = httpClient;
        }

        public async Task ProcessAsync(QueueItem item, CancellationToken cancellationToken)
        {
            var subscriptions = await _dbContext.Subscriptions
                .Where(s => s.Key == item.Message.Key)
                .AsNoTracking()
                .ToArrayAsync();

            List<Exception> failures = new List<Exception>();
            foreach (var subscription in subscriptions)
            {
                var failure = await _procesSubscriptionMessage(subscription, item);
                if (failure != default)
                {
                    failures.Add(failure);
                }
            }

            var status = QueuedMessageStatus.Queued;
            if (failures.Count == 0)
            {
                status = QueuedMessageStatus.Completed;
            }

            var queueMessage = new QueuedMessage
            {
                Id = item.Id
            };

            _dbContext.Attach(queueMessage);
            queueMessage.Status = status;

            await _dbContext.SaveChangesAsync();

            if (status != QueuedMessageStatus.Completed)
            {
                throw new AggregateException(failures);
            }
        }

        private async Task<Exception> _procesSubscriptionMessage(Subscription subscription, QueueItem item)
        {
            var statusChange = await _dbContext.QueuedMessageSubscriptions
                .Where(o => o.SubscriptionId == subscription.Id)
                .Where(o => o.QueuedMessageId == item.Id)
                .FirstOrDefaultAsync();

            if (statusChange == default)
            {
                statusChange = new QueuedMessageSubscription
                {
                    SubscriptionId = subscription.Id,
                    QueuedMessageId = item.Id,
                    Status = MessageSubscriptionStatus.Queued
                };

                _dbContext.Add(statusChange);
            }
            else
            {
                if (statusChange.Status == MessageSubscriptionStatus.Completed)
                {
                    return default;
                }
            }

            if (!subscription.IsActive)
            {
                statusChange.Status = MessageSubscriptionStatus.Unsend;
                return default;
            }

            try
            {
                var content = _getContent(subscription, item.Message);

                var response = await _httpClient.PostAsync(subscription.Url, content);
                response.EnsureSuccessStatusCode();

                statusChange.Status = MessageSubscriptionStatus.Completed;
                return default;
            }
            catch (Exception ex)
            {
                return ex;
            }
        }

        private static readonly HashSet<string> _formContentTypes = new HashSet<string>(StringComparer.OrdinalIgnoreCase)
        {
            "application/x-www-form-urlencoded",
            "multipart/form-data"
        };

        private HttpContent _getContent(Subscription subscription, IMessage message)
        {
            if (_formContentTypes.Contains(message.ContentType))
            {
                MultipartFormDataContent multipartFormData = new MultipartFormDataContent();

                if (subscription.ApiKeyMethod == ApiKeyMethod.Body)
                {
                    multipartFormData.Add(new StringContent(subscription.ApiKey), nameof(subscription.ApiKey));
                }
                
                foreach (var pair in JObject.Parse(message.Content))
                {
                    if (pair.Value is JArray array)
                    {
                        foreach (var item in array)
                        {
                            multipartFormData.Add(new StringContent(item.ToString()), pair.Key);
                        }
                    }
                    else
                    {
                        multipartFormData.Add(new StringContent(pair.Value.ToString()), pair.Key);
                    }
                }

                return multipartFormData;
            }
            else
            {
                var jsonObject = JObject.Parse(message.Content);
                if (subscription.ApiKeyMethod == ApiKeyMethod.Body)
                {
                    jsonObject[nameof(subscription.ApiKey)] = subscription.ApiKey;
                }

                return new StringContent(jsonObject.ToString(Formatting.None), Encoding.UTF8, message.ContentType);
            }
        }
    }
}
