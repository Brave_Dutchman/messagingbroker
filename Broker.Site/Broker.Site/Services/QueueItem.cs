﻿using Broker.Libaries.Data.Models;
using System;

namespace Broker.Site.Services
{
    public class QueueItem
    { 
        public QueueItem(int id, IMessage message, DateTime queuedAt, IQueuedMessageHistory lastHistoryLine) 
            : this(id, message, queuedAt)
        {
            LastHistoryLine = lastHistoryLine;
        }

        public QueueItem(int id, IMessage message, DateTime queuedAt)
        {
            Id = id;
            Message = message;
            QueuedAt = queuedAt;
        }

        public int Id { get; set; }

        public IMessage Message { get; }

        public DateTime QueuedAt { get; }

        public DateTime? LastErrorAt { get; set; }

        public int ErrorCount { get; set; }

        public IQueuedMessageHistory LastHistoryLine { get; }
    }
}
