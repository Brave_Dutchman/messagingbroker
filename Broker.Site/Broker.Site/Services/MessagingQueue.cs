﻿using Broker.Libaries.Data;
using Broker.Libaries.Data.Models;
using Broker.Libaries.Data.Models.Enums;
using System;
using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Tasks;

namespace Broker.Site.Services
{
    public class MessagingQueue
    {
        private readonly ConcurrentQueue<QueueItem> _messages = new ConcurrentQueue<QueueItem>();
        private readonly SemaphoreSlim _signal = new SemaphoreSlim(0);

        public async Task PushMessageAsync(IMessage message, BrokerDbContext dbContext)
        {
            if (message == null)
            {
                throw new ArgumentNullException(nameof(message));
            }

            var queuedMessage = new QueuedMessage()
            {
                QueuedOn = DateTime.Now,
                MessageId = message.Id,
                ErrorCount = 0,
                Status = QueuedMessageStatus.Queued
            };

            dbContext.Add(queuedMessage);
            await dbContext.SaveChangesAsync();

            PushQueueItem(new QueueItem(queuedMessage.Id, message, DateTime.Now));
        }

        public async Task PushRequeueAsync(QueuedMessage queuedMessage, BrokerDbContext dbContext)
        {
            if (queuedMessage == null)
            {
                throw new ArgumentNullException(nameof(queuedMessage));
            }

            if (queuedMessage.Id == default)
            {
                throw new ArgumentException("QueuedMessage must be an existsing database object");
            }
            
            var historyLine = new QueuedMessageHistoryLine
            {
                QueuedOn = queuedMessage.QueuedOn,
                ArchivedOn = DateTime.Now,
                ErrorCount = queuedMessage.ErrorCount,
                LastErrorOn = queuedMessage.LastErrorOn,
                Status = queuedMessage.Status,
                QueuedMessageId = queuedMessage.Id
            };

            queuedMessage.QueuedOn = historyLine.ArchivedOn;
            queuedMessage.LastErrorOn = null;
            queuedMessage.ErrorCount = 0;
            queuedMessage.Status = QueuedMessageStatus.Queued;

            dbContext.Add(historyLine);
            await dbContext.SaveChangesAsync();

            PushQueueItem(new QueueItem(queuedMessage.Id, queuedMessage.Message, DateTime.Now, historyLine));
        }

        public void PushQueueItem(QueueItem message)
        {
            if (message == null)
            {
                throw new ArgumentNullException(nameof(message));
            }

            _messages.Enqueue(message);
            _signal.Release();
        }

        public async Task<QueueItem> DequeueAsync(CancellationToken cancellationToken)
        {
            await _signal.WaitAsync(cancellationToken);
            _messages.TryDequeue(out var message);

            return message;
        }
    }
}
