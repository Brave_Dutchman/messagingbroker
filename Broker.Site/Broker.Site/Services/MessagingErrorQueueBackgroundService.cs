﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Broker.Site.Services
{
    public class MessagingErrorQueueBackgroundService : BackgroundService
    {
        private readonly ILogger _logger;
        private readonly MessagingErrorQueue _messagingErrorQueue;
        private readonly MessagingQueue _messagingQueue;

        private readonly static TimeSpan _defaultOffset = new TimeSpan(0, 0, 4);
        private readonly static int _maxErrors = 5;

        public MessagingErrorQueueBackgroundService(MessagingQueue messagingQueue, ILogger<MessagingErrorQueueBackgroundService> logger, MessagingErrorQueue messagingErrorQueue)
        {
            _messagingQueue = messagingQueue;
            _logger = logger;
            _messagingErrorQueue = messagingErrorQueue;
        }

        protected override async Task ExecuteAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("MessagingErrorProcessor is starting.");

            while (!cancellationToken.IsCancellationRequested)
            {
                try
                {
                    var errorMessage = await _messagingErrorQueue.DequeueAsync(cancellationToken);
                    if (errorMessage == default)
                    {
                        continue;
                    }

                    if (errorMessage.ErrorCount >= _maxErrors)
                    {
                        _logger.LogWarning($"Delivering of message: '{errorMessage.Message.Id}' failed '{errorMessage.ErrorCount}' times");
                        await _messagingErrorQueue.PushErrorAsync(errorMessage);
                    }
                    else
                    {
                        var task = Task.Run(async () =>
                        {
                            try
                            {
                                var waitTime = errorMessage.LastErrorAt.Value + _defaultOffset - DateTime.Now;
                                if (waitTime > TimeSpan.Zero)
                                {
                                    await Task.Delay(waitTime, cancellationToken);
                                }

                                _messagingQueue.PushQueueItem(errorMessage);
                            }
                            catch (TaskCanceledException ex)
                            {
                                _logger.LogInformation(ex, "MessagingProcessor wait Task Canceled");
                            }
                            catch (Exception ex)
                            {
                                _logger.LogError(ex, $"Error occurred.");
                            }
                        });
                    }
                }
                catch (TaskCanceledException ex)
                {
                    _logger.LogInformation(ex, "MessagingProcessor Task Canceled");
                    break;
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"Error occurred executing.");
                }
            }

            _logger.LogInformation("MessagingErrorProcessor is stopping.");
        }
    }
}
