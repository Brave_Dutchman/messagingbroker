﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace Broker.Site.Middleware
{
    public class ExceptionHandleMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger<ExceptionHandleMiddleware> _logger;

        public ExceptionHandleMiddleware(RequestDelegate next, ILogger<ExceptionHandleMiddleware> logger)
        {
            _next = next;
            _logger = logger;
        }

        public async Task Invoke(HttpContext httpContext)
        {
            try
            {
                await _next(httpContext);
            }
            catch (Exception ex)
            {
                var requestId = httpContext.TraceIdentifier;
                _logger.LogError(ex, $"id: '{requestId}', Unhandled exception");
                try
                {
                    var body = await httpContext.Request.ReadBodyAsync();
                    _logger.LogError($"id: '{requestId}', body: '{body}'");
                }
                catch (Exception)
                {
                    try
                    {
                        var body = await httpContext.Request.ReadBodyAsString();
                        _logger.LogError($"id: '{requestId}', body: '{body}'");
                    }
                    catch (Exception)
                    {
                        _logger.LogError($"id: '{requestId}', could not read request body");
                    }
                }
            }
        }
    }
}
