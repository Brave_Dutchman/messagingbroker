﻿using Broker.Libaries.Data;
using Broker.Libaries.Data.Models;
using Broker.Libaries.Data.Models.Enums;
using Broker.Site.Controllers;
using Broker.Site.Models.Queries;
using Broker.Site.Models.Requests;
using Broker.Site.Services;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace Broker.Site.Apis
{
    public class RequeueQueueApi
    {
        private readonly BrokerDbContext _dbContext;
        private readonly MessagingQueue _messagingQueue;

        public RequeueQueueApi(BrokerDbContext dbContext, MessagingQueue messagingQueue)
        {
            _dbContext = dbContext;
            _messagingQueue = messagingQueue;
        }

        public async Task RequeueUnsend(SubscriberQueryModel subscription)
        {
            var messages = await _dbContext.QueuedMessageSubscriptions
                .Where(qms => qms.Status == MessageSubscriptionStatus.Unsend)
                .Where(m => m.SubscriptionId == subscription.Id)
                .Select(qms => qms.QueuedMessage)
                .Include(qm => qm.Message)
                .AsNoTracking()
                .ToArrayAsync();

            foreach (var message in messages)
            {
                await _messagingQueue.PushRequeueAsync(message, _dbContext);
            }
        }

        public async Task RequeueError(PublisherQueryModel publisher, RequeueErrorRequest model)
        {
            var query = _dbContext.QueuedMessages
                .Include(qm => qm.Message)
                .Where(m => m.Status == QueuedMessageStatus.Error)
                .Where(m => m.Message.PublisherId == publisher.Id)
                .Where(m => m.Message.Key == model.Key);

            if (model.From != default)
            {
                query = query.Where(m => m.Message.CreatedOn >= model.From.Value);
            }

            var messages = await query
                .AsNoTracking()
                .ToArrayAsync();

            foreach (var message in messages)
            {
                await _messagingQueue.PushRequeueAsync(message, _dbContext);
            }
        }
    }
}
