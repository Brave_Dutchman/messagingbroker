﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace Broker.TestApp.Controllers
{
    [ApiController]
    [Route("reciever")]
    public class RecieverController : Controller
    {
        private readonly ILogger<RecieverController> _logger;

        public RecieverController(ILogger<RecieverController> logger)
        {
            _logger = logger;
        }
        
        [HttpPost("test")]
        public IActionResult Test(TestObject testObject)
        {
            _logger.LogInformation("recieved: " + JsonConvert.SerializeObject(testObject));
            return Ok();
        }

        [HttpPost("test_404")]
        public IActionResult Test_404(TestObject testObject)
        {
            _logger.LogInformation("recieved: " + JsonConvert.SerializeObject(testObject));
            return NotFound();
        }
    }

    public class TestObject
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
