﻿using Broker.Libaries.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace Broker.Libaries.Data
{
    public class BrokerDbContext : DbContext
    {
        public BrokerDbContext(DbContextOptions options) : base(options)
        { }

        public DbSet<Publisher> Publishers { get; set; }
        public DbSet<Subscription> Subscriptions { get; set; }

        public DbSet<Message> Messages { get; set; }
        public DbSet<QueuedMessage> QueuedMessages { get; set; }

        public DbSet<QueuedMessageSubscription> QueuedMessageSubscriptions { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
            optionsBuilder.EnableSensitiveDataLogging();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Publisher>().Property(p => p.ApiKey).HasMaxLength(100);
            modelBuilder.Entity<Publisher>().HasAlternateKey(p => p.ApiKey);
            modelBuilder.Entity<Publisher>().HasIndex(p => p.ApiKey).ForSqlServerInclude(nameof(Publisher.Id));

            modelBuilder.Entity<Message>().Property(p => p.Key).HasMaxLength(100).IsRequired();
            modelBuilder.Entity<Message>().Property(p => p.ContentType).HasMaxLength(255).IsRequired();
            modelBuilder.Entity<Message>().Property(p => p.Content).IsRequired();
            
            modelBuilder.Entity<Subscription>().HasIndex(p => new { p.Key, p.IsActive });

            modelBuilder.Entity<Subscription>().Property(e => e.Name).HasMaxLength(100);
            modelBuilder.Entity<Subscription>().HasIndex(p => new { p.Name }).IsUnique();

            modelBuilder.Entity<Publisher>().Property(e => e.Name).HasMaxLength(100);
            modelBuilder.Entity<Publisher>().HasIndex(p => new { p.Name }).IsUnique();
        }
    }

    public class BloggingContextFactory : IDesignTimeDbContextFactory<BrokerDbContext>
    {
        public BrokerDbContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<BrokerDbContext>();
            optionsBuilder.UseSqlServer("Server=(localdb)\\mssqllocaldb;Database=BrokerDatabase;Trusted_Connection=True;");

            return new BrokerDbContext(optionsBuilder.Options);
        }
    }
}
