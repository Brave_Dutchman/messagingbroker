﻿using System;

namespace Broker.Libaries.Data.Models
{
    public class Subscription
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Key { get; set; }

        public DateTime CreatedOn { get; set; }
        public DateTime UpdatedOn { get; set; }

        public bool IsActive { get; set; }

        public string Url { get; set; }
        public string ApiKey { get; set; }
        public ApiKeyMethod ApiKeyMethod { get; set; }
    }
}
