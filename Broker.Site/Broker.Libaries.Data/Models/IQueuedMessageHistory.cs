﻿using Broker.Libaries.Data.Models.Enums;
using System;

namespace Broker.Libaries.Data.Models
{
    public interface IQueuedMessageHistory
    {
        DateTime QueuedOn { get; }

        DateTime? LastErrorOn { get; }

        QueuedMessageStatus Status { get; }

        int ErrorCount { get; }
    }
}
