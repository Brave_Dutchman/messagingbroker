﻿using System;

namespace Broker.Libaries.Data.Models
{
    public class Message : IMessage
    {
        public int Id { get; set; }

        public string Key { get; set; }

        public string Content { get; set; }

        public string ContentType { get; set; }

        public DateTime CreatedOn { get; set; }

        public int PublisherId { get; set; }
        public Publisher Publisher { get; set; }
    }
}
