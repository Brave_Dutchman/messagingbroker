﻿using Broker.Libaries.Data.Models.Enums;

namespace Broker.Libaries.Data.Models
{
    public class QueuedMessageSubscription
    {
        public int Id { get; set; }

        public MessageSubscriptionStatus Status { get; set; }

        public int QueuedMessageId { get; set; }
        public virtual QueuedMessage QueuedMessage { get; set; }

        public int SubscriptionId { get; set; }
        public virtual Subscription Subscription { get; set; }
    }
}
