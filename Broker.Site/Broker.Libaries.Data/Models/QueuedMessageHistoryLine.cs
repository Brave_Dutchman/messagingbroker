﻿using Broker.Libaries.Data.Models.Enums;
using System;

namespace Broker.Libaries.Data.Models
{
    public class QueuedMessageHistoryLine : IQueuedMessageHistory
    {
        public int Id { get; set; }

        public DateTime QueuedOn { get; set; }

        public DateTime ArchivedOn { get; set; }

        public DateTime? LastErrorOn { get; set; }

        public QueuedMessageStatus Status { get; set; }

        public int ErrorCount { get; set; }

        public int QueuedMessageId { get; set; }
        public virtual QueuedMessage QueuedMessage { get; set; }
    }
}
