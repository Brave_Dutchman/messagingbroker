﻿using Broker.Libaries.Data.Models.Enums;
using System;
using System.Collections.Generic;

namespace Broker.Libaries.Data.Models
{
    public class QueuedMessage : IQueuedMessageHistory
    {
        public int Id { get; set; }

        public DateTime QueuedOn { get; set; }

        public DateTime? LastErrorOn { get; set; }

        public QueuedMessageStatus Status { get; set; }

        public int ErrorCount { get; set; }

        public int MessageId { get; set; }

        public virtual Message Message { get; set; }

        public virtual ICollection<QueuedMessageSubscription> Subscriptions { get; set; }

        public virtual ICollection<QueuedMessageHistoryLine> History { get; set; }
    }
}
