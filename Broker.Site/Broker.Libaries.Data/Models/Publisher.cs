﻿using System;

namespace Broker.Libaries.Data.Models
{
    public class Publisher
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public DateTime CreatedOn { get; set; }
        public DateTime UpdatedOn { get; set; }

        public string ApiKey { get; set; }
    }
}
