﻿namespace Broker.Libaries.Data.Models.Enums
{
    public enum MessageSubscriptionStatus
    {
        Initial = 0,
        Queued = 10,
        Error = 15,
        Unsend = 18,
        Completed = 20
    }
}
