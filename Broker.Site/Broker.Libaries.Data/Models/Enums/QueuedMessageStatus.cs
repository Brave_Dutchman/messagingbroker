﻿namespace Broker.Libaries.Data.Models.Enums
{
    public enum QueuedMessageStatus
    {
        Initial = 0,
        Queued = 10,
        Error = 15,
        Completed = 20,
    }
}
