﻿using System;

namespace Broker.Libaries.Data.Models
{
    public interface IMessage
    {
        int Id { get; }
        DateTime CreatedOn { get; }
        string Content { get; }
        string ContentType { get; }
        string Key { get; }
        int PublisherId { get; }
    }
}